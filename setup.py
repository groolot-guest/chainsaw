#!/usr/bin/env python

import os
import re
from setuptools import setup

with open(os.path.join('chainsaw', '__init__.py'), encoding='utf-8') as f:
    version = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", f.read(), re.M).group(1)
    f.seek(0)
    author = re.search(r"^__author__ = ['\"]([^'\"<>]*).*['\"]", f.read(), re.M).group(1)
    f.seek(0)
    author_email = re.search(r"^__author__ = ['\"][^'\"<>]+([^'\">]+)>['\"]", f.read(), re.M).group(1)
    f.seek(0)
    license = re.search(r"^__license__ = ['\"]([^'\"]*)['\"]", f.read(), re.M).group(1)

if not version:
    raise RuntimeError('Cannot find Chainsaw version information.')

if not author:
    raise RuntimeError('Cannot find Chainsaw author information.')

if not author_email:
    raise RuntimeError('Cannot find Chainsaw author email information.')

if not license:
    raise RuntimeError('Cannot find Chainsaw license information.')

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "chainsaw",
    version = version,
    author = author,
    author_email = author_email,
    description = ("Audio time-sampler and snapshot looper/granulator"),
    long_description=read('README.md'),
    license = license,
    keywords = "audio sampler looper granulator",
    url = "https://framagit.org/groolot-association/chainsaw",
    install_requires = [
        'pyo',
        'liblo',
    ],
    packages=[
        'chainsaw',
        'midi_chainsaw'
    ],
    entry_points={"console_scripts": [
        "chainsaw=chainsaw:main",
        "midi_chainsaw=midi_chainsaw:main",
    ]},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Sound",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Artistic Software",
        "Topic :: Multimedia :: Sound/Audio :: Capture/Recording",
        "Topic :: Multimedia :: Sound/Audio :: MIDI",
    ],
)
